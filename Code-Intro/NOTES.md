# Errors & improvements for the text

## http://pages.cs.wisc.edu/~remzi/OSTEP/intro.pdf (v.0.92)

### p. 3

```sh
gcc -o cpu cpu.c -Wall
```

On my machine (gcc (Debian 6.3.0-8)), an explicit `-lpthread` is needed.

### p. 4

```sh
./cpu A & ; ./cpu B & ; ./cpu C & ; ./cpu D &
```

Incorrect with `bash`. Should be:

```sh
./cpu A &  ./cpu B &  ./cpu C &  ./cpu D &
```

Better to mention also the command to kill them (`bash`)

```sh
kill %1 %2 %3 %4
```

or (`psmisc` utils)

```sh
kill --exact ./cpu
```

It is also worth noting that if one has a number of cores $\geq 4$, the scheduling
of the four processes is deterministic with a high probability. This is
misleading and does not correspond to the output shown in the text. One can have
a more interesting output with a number of processes greater than the number of
the cores.

## p. 5

```sh
./mem
```

The current version of the code
(http://pages.cs.wisc.edu/~remzi/OSTEP/Code/code.intro.tgz) requires an
argument.

```sh
./mem 0
```

Also, modern Linux machines have *address space layout randomization* (ASLR), a
security measure designed to make memory addresses difficult to predict (and
different for each run). To disable ASLR in a `bash` session:

```sh
setarch $(uname -m) -RL bash
```

### p. 7

`threads.c` should be `threads.v0.c`

On my machine 100000 is too low to get a data race.

### p. 9

`io.c` doesn't compile without `sys/stat.h` 

```c
#include <sys/stat.h>
```

